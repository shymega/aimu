use anyhow::{Error, Result};
use evdev::{self, AbsoluteAxisType, Device, InputEventKind};
use serde::{Deserialize, Serialize};
use std::{
    sync::{Arc, Mutex},
    thread,
};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
#[cfg(feature = "cli")]
#[derive(clap::ValueEnum)]
#[value(rename_all = "lowercase")]
pub enum EventCode {
    AbsZ,
}

//TODO: support code combos, e.g., ABS_RX|ABS_RY
impl From<EventCode> for InputEventKind {
    fn from(val: EventCode) -> Self {
        match val {
            EventCode::AbsZ => InputEventKind::AbsAxis(AbsoluteAxisType::ABS_Z),
            _ => panic!("unsupported event code!"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub device: Option<String>,
    pub event: EventCode,
    pub thresh: i32,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            device: Some(String::from("Microsoft X-Box 360 pad")),
            event: EventCode::AbsZ,
            thresh: 10,
        }
    }
}

pub struct Trigger {
    device: Option<Arc<Mutex<Device>>>,
    event: Arc<Mutex<InputEventKind>>,
    thresh: i32,
    state: Arc<Mutex<i32>>,
}

impl TryFrom<Config> for Trigger {
    type Error = Error;
    fn try_from(cfg: Config) -> Result<Self> {
        Ok(Self {
            device: cfg.device.and_then(|name| {
                evdev::enumerate()
                    .map(|t| t.1)
                    .find(|d| d.name().unwrap() == name)
                    .map(|d| Arc::new(Mutex::new(d)))
            }),
            event: Arc::new(Mutex::new(cfg.event.into())),
            thresh: cfg.thresh,
            state: Arc::new(Mutex::new(0)),
        })
    }
}

impl Trigger {
    pub fn task(&self) {
        let state = Arc::clone(&self.state);
        match &self.device {
            Some(dev) => {
                let device = Arc::clone(dev);
                let event = Arc::clone(&self.event);
                thread::spawn(move || loop {
                    for e in device.lock().unwrap().fetch_events().unwrap() {
                        if e.kind() == *event.lock().unwrap() {
                            *(state.lock().unwrap()) = e.value();
                        };
                    }
                });
            }
            None => *(state.lock().unwrap()) = self.thresh,
        };
    }

    pub fn check(&self) -> bool {
        *self.state.lock().unwrap() >= self.thresh
    }
}
